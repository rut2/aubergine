# aubergin

a [Covid API v1](https://documenter.getpostman.com/view/3193762/TVCiV7aG#282b4b9b-1905-4224-a293-b73251c8fc1a) application


### How to run
1. This project is build on sails js, [install sails js globally](https://sailsjs.com/get-started)
2. run `npm install` to install all dependency in project root folder
3. once all the dependencies are installed, run `sails lift`



### Important Notes 
+ API authentication works on authorization header in each request, you need to pass registered userId  
+ Create user API is open 
+ Check [API documentation](https://documenter.getpostman.com/view/3193762/TVCiV7aG#282b4b9b-1905-4224-a293-b73251c8fc1a) for more details
