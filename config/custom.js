/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.custom = {

  /***************************************************************************
  *                                                                          *
  * Any other custom config this Sails app should use during development.    *
  *                                                                          *
  ***************************************************************************/
  mailgunDomain: 'sandboxb55085ee60034317af866a7f7c218082.mailgun.org',
  mailgunSecret: 'da518fd9f5b5c233d77b3d3e73575271-f877bd7a-093b1e0a',
  // …

};
