const plotly = require('plotly')('rutvik.prajapati', 'IRvvR8qrEPVsZxXXJOmh');
const fs = require('fs');

module.exports = {


  friendlyName: 'Send mail',


  description: '',


  inputs: {
    country: {
      type: 'string',
      required: true
    }
  },


  exits: {
    success: {
      description: 'New user account was created successfully.'
    },

  },


  fn: async function (inputs, exits) {

    let userRecord = await Users.findOne({id: this.req.headers.authorization});

    let rawData = await sails.helpers.covid.getReport.with({country: inputs.country});

    let data = [{
      x: ['deaths', 'confirmed', 'recovered'],
      y: [rawData.data.latest_data.deaths, rawData.data.latest_data.confirmed, rawData.data.latest_data.recovered],
      type: 'bar'
    }];

    let imgOpts = {
      format: 'png',
      width: 1000,
      height: 500
    };

    plotly.getImage({data}, imgOpts, async (error, imageStream) => {
      if (error) {
        return console.log(error);
      }
      const fileName = `${Date.now()}.png`;
      const filePath = `./assets/charts/${fileName}`;

      let fileStream = await fs.createWriteStream(filePath);

      await imageStream.pipe(fileStream);

      setTimeout(async () => {
        await sails.helpers.mail.send.with({
          to: userRecord.emailAddress,
          from: 'Rutvik Prajapati <rut2prajapati@gmail.com>',
          subject: 'Covid statistics',
          text: 'Be safe in aware of COVID',
          attachment: fileName
        });
      }, 3000);


    });

    await exits.success();

  }

};
