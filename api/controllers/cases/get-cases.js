const moment = require('moment');


module.exports = {


  friendlyName: 'Get cases',


  description: '',


  inputs: {
    startAt: {
      type: 'string',
      required: false,
      custom: value => moment(value).isValid()
    },

    endAt: {
      type: 'string',
      required: false,
      custom: value => moment(value).isValid()
    },

    country: {
      type: 'string',
      required: false
    }
  },


  exits: {},


  fn: async function (inputs) {

    let userRecord = await Users.findOne({id: this.req.headers.authorization});

    const country = inputs.country !== undefined ? inputs.country : userRecord.country;

    let rawData = await sails.helpers.covid.getReport.with({country});
    let finalResponse = [];

    if (inputs.startAt === undefined || inputs.endAt === undefined) {
      finalResponse = rawData.data.timeline.slice(0, 15);
    } else {

      // rather then checking date recursively on object, make an index array
      let datesArray = _.map(rawData.data.timeline, 'date');


      datesArray.forEach((date, index) => {
        if (moment(date).isBetween(moment(inputs.startAt).format('YYYY-MM-DD'), moment(inputs.endAt).format('YYYY-MM-DD'), null, '[]')) {
          finalResponse.push(rawData.data.timeline[index]);
        }
      });

    }

    await this.res.json(finalResponse);
  }


};
