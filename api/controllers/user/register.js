module.exports = {


  friendlyName: 'Register',


  description: 'Register user.',


  inputs: {
    firstName: {
      type: 'string',
      required: true
    },

    lastName: {
      type: 'string',
      required: true
    },

    emailAddress: {
      type: 'string',
      required: true,
      isEmail: true,
      unique: true
    },

    password: {
      type: 'string',
      required: true
    },

    country: {
      type: 'string',
      required: true
    }
  },


  exits: {
    invalid: {
      responseType: 'badRequest',
      description: 'The provided fullName, password and/or email address are invalid.'
    },

    emailAlreadyInUse: {
      statusCode: 409,
      description: 'The provided email address is already in use.',
    }
  },


  fn: async function (inputs) {

    let newUserRecord = await Users.create({
      firstName: inputs.firstName,
      lastName: inputs.lastName,
      emailAddress: inputs.emailAddress.toLowerCase(),
      password: await sails.helpers.passwords.hashPassword(inputs.password),
      country: inputs.country
    })
      .intercept('E_UNIQUE', 'emailAlreadyInUse')
      .intercept({name: 'UsageError'}, 'invalid')
      .fetch();

    await this.res.json(newUserRecord);
  }
};
