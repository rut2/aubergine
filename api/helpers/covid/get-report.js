const axios = require('axios');

module.exports = {


  friendlyName: 'Get report',


  description: '',


  inputs: {
    country: {
      type: 'string',
      required: true
    }
  },


  exits: {

    success: {
      outputFriendlyName: 'Covid Report',
    },

  },


  fn: async function (inputs) {
    try {
      return (await axios.get('https://corona-api.com/countries/' + inputs.country)).data;
    } catch (e) {
      console.log('error in from corona-api');
    }
  }


};

