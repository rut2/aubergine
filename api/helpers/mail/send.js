const path = require('path');
const fs = require('fs');

module.exports = {


  friendlyName: 'Send',


  description: 'Send mail.',


  inputs: {
    to: {
      type: 'string',
      isEmail: true,
      required: true
    },

    from: {
      type: 'string',
      required: true
    },

    subject: {
      type: 'string',
      required: true
    },

    text: {
      type: 'string',
      required: true
    },

    attachment: {
      type: 'string',
      required: false
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {

    const mailgun = require('mailgun-js');
    console.log(inputs.attachment);

    let file = fs.readFileSync(path.join('assets','charts', inputs.attachment));



    const DOMAIN = sails.config.custom.mailgunDomain;
    const mg = mailgun({apiKey: sails.config.custom.mailgunSecret, domain: DOMAIN});

    const attch = new mg.Attachment({data: file, filename: inputs.attachment});

    const data = {
      from: inputs.from,
      to: inputs.to,
      subject: inputs.subject,
      text: inputs.text,
      attachment: attch
    };

    console.log(data);

    mg.messages().send(data, (error, body) => {
      console.log(body);
    });


  }


};

