/**
 * is-authenticated
 *
 * A simple policy that allows any request from an authenticated user.
 *
 * For more about how to use policies, see:
 *   https://sailsjs.com/config/policies
 *   https://sailsjs.com/docs/concepts/policies
 *   https://sailsjs.com/docs/concepts/policies/access-control-and-permissions
 */

module.exports = async function (req, res, proceed) {

  // if authorization token does not exits then request is unauthorized
  if(!req.headers.authorization) {
    return res.unauthorized();
  }

  let accessToken = req.headers.authorization;

  let userRecord = await Users.findOne({id: accessToken});

  if(!userRecord) {
    return res.unauthorized();
  }

  return proceed();
};
